'use strict';

jQuery(document).ready(function ($) {
    $('.Info').hide();
    $('.Info:first').show();
    $('.serviceMenu:first').addClass('active activeTab');
    $('.serviceLink:first').addClass('whiteText');

    $(' .serviceMenu').click(function (event) {
        $(' .serviceMenu').removeClass('active activeTab ');
        $(this).addClass('active activeTab ');
        $('.Info').hide();

        let selectTab= $(this).find('a').attr("href");
        $(selectTab).fadeIn();
        $('.serviceLink').removeClass('whiteText');
        $(this).find('.serviceLink').addClass('whiteText');



    });

    $('.photoBlock').hide();
    $('.photoBlock:first').show();
    $('.workMenu:first').addClass('workMenuActive');
    $('.workLink:first').addClass('greenText');

    $('.workMenu').click(function (event2){
        $(' .workMenu').removeClass('workMenuActive ');
        $(this).addClass('workMenuActive ');
        $('.photoBlock').hide();
        let selectBlock= $(this).find('a').attr("href");
        $(selectBlock).fadeIn();
        $('.workLink').removeClass('greenText');
        $(this).find('.workLink').addClass('greenText');



    });

        $('.photoContainer').hide();
        $('.photoContainer:gt(11)').show();
        $('.loadButton').click(function (event3) {
        $('.photoContainer').show();
        $('.loadButton').addClass('activeButton') ;

    });
     $('.photoContainer').hover(function () {
         $(this).addClass('.photoContainerHover');
     });







    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        centerMode: true,
        arrows:true,
        focusOnSelect: true
    });




});



